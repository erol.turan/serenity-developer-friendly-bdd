# serenity-developer-friendly-bdd

This is a showcase using the framework serenity bdd for a developer friendly bdd approach

## Getting Started

The project itself uses gradle-wrapper, so it only needs java to run.

Open the command line and run with following command:
```
./gradlew clean test aggregate
```
For further details open the serenity reference manual.

## Author

* **Erol Turan**

