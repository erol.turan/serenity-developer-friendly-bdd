package sr.dv.fr;

import net.thucydides.core.annotations.Step;

import static org.assertj.core.api.Assertions.assertThat;


public class CalculatorSteps {

    private Calculator calculator = new Calculator();
    private int x = 0;
    private int y = 0;
    private int result = 0;

    @Step("Given x is set to {0}")
    public void x_is_set_to(int x) {
        this.x = x;
    }

    @Step("Given y is set to {0}")
    public void y_is_set_to(int y) {
        this.y = y;
    }

    @Step("When the calculator adds x to y")
    public void the_calculator_adds_x_to_y() {
        result = calculator.add(x, y);
    }

    @Step("Then the result is {0}")
    public void the_result_is(int result) {
        assertThat(result).isEqualTo(this.result);
    }


}
