package sr.dv.fr;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityRunner.class)
public class CalculatorTest {


    @Steps
    CalculatorSteps calculatorSteps;

    @Test
    public void addNumbersToCorrectResult() {
        // GIVEN
        calculatorSteps.x_is_set_to(3);

        // AND
        calculatorSteps.y_is_set_to(5);

        // WHEN
        calculatorSteps.the_calculator_adds_x_to_y();

        // THEN
        calculatorSteps.the_result_is(8);


    }

    @Test
    public void addNumbersToWrongResult() {
        // GIVEN
        calculatorSteps.x_is_set_to(3);

        // AND
        calculatorSteps.y_is_set_to(5);

        // WHEN
        calculatorSteps.the_calculator_adds_x_to_y();

        // THEN
        calculatorSteps.the_result_is(9);


    }
}
